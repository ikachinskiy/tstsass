<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Тест SASS</title>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        <div id="container">
            <header>
                <h1>Привет</h1>
            </header>
            <div id="content">
                <h1>Заголовок 1</h1>
                <p>Текст сообщения...</p>
            </div>
            <footer>
                <p>Копилефт</p>
            </footer>
        </div>
    </body>
</html>
